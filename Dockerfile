# Use Docker to figure out errors in the BitBucket pipeline

FROM perl:latest as dependencies

WORKDIR /var/test

RUN export LC_ALL=C.UTF-8 && \
    cpanm --quiet --notest Module::Build

FROM dependencies as author

RUN export LC_ALL=C.UTF-8 && \
    apt-get update && \
    apt-get install -y ispell && \
    cpanm --quiet --notest Module::Signature Test::Pod Test::Pod::Coverage \
        Test::TestCoverage Test::Perl::Critic Test::Kwalitee Devel::Cover \
        Test::Spelling Perl::Critic::StricterSubs Perl::Critic::Nits \
        Test::Dependencies Perl::Critic::Bangs Test::NoWarnings

FROM author

COPY . .

RUN export LC_ALL=C.UTF-8 && \
    cpanm --quiet --notest --installdeps .

ENV DEVEL_COVER_OPTIONS "-silent,1"

RUN cd /var/test && \
    perl Build.PL && \
    ./Build && \
    rm Dockerfile && \
    rm -Rf blib && \
    rm -Rf .vscode

CMD ["./Build", "authortestcover"]